module.exports = {
  onboardingConfig: {
    extends: ["config:base"],
  },
  platform: "gitlab",
  gitAuthor: "RenovateBot <renovatebot@gmail.com>",
  baseBranches: ["main","master"],
  labels: ["dependencies"],
  repositories: [
    "jeremiahflaga/renovate-bot",
    "jeremiahflaga/sample-nodejs-app",
    "jeremiahflaga/sample-nodejs-app-2"
  ],
};
